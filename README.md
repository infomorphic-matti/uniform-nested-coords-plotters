# uniform-nested-coords-plotters
Small module for [plotters](https://github.com/plotters-rs/plotters) that provides similar functionality to `plotters::coord::combinators::NestedRange` but where the nested coordinates are uniform (the implementation in `plotters` internally holds a vector for each category). 
